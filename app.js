/**
 * Main entry point for the app.
 * First we import the document from google drive.
 * We load the form into the page.
 * User inputs data into the PDF form.
 * Save data to Localstorage, to repopulate and track the form.
 */

// Google Docs API
// Display PDF lib
// PDF to JSON
// JSON to LocalStorage (or database)
// Reload handler to repopulate form on reload.
// Some way to track seperate versions of the form.

